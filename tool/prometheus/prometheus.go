package prometheus

import (
	"github.com/micro/go-micro/util/log"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"net/http"
)

// NewProme service monitoring tools
func NewProme(addr string) {
	http.Handle("/metrics", promhttp.Handler())
	go func() {
		err := http.ListenAndServe(addr, nil)
		if err != nil {
			log.Error("ListenAndServe: addr is :", addr, "err :", err)
		}
	}()
}
